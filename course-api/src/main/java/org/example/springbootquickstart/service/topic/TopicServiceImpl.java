package org.example.springbootquickstart.service.topic;

import org.example.springbootquickstart.domain.Topic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {


    private List<Topic> topics =new ArrayList<>(
            Arrays.asList(
                    new Topic("spring","Spring Framework","Spring Framework Description"),
                    new Topic("java","Core Java","Core Java Description"),
                    new Topic("javascript","JavaScript","JavaScript Description")
            )
    );

    @Override
    public List<Topic> getAllTopics() {
        return topics;
    }

    @Override
    public Topic getTopic(String id) {
        return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
    }

    @Override
    public void addTopic(Topic topic) {
        topics.add(topic);
    }

    @Override
    public void updateTopic(String id, Topic topic) {
        for (int i = 0; i < topics.size(); i++){
            Topic t = topics.get(i);
            if(t.getId().equals(id)){
                topics.set(i, topic);
                return;
            }
        }
    }

    @Override
    public void deleteTopic(String id) {
        topics.removeIf(t -> t.getId().equals(id));
    }
}
