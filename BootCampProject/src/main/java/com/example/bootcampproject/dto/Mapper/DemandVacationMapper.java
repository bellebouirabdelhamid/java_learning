package com.example.bootcampproject.dto.Mapper;


import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import com.example.bootcampproject.dto.DemandVacationDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class DemandVacationMapper {
    public abstract DemandVacationDTO toDto (DemandVacation demandVacation);
    public abstract List<DemandVacationDTO> toDtoList (List<DemandVacation> demandVacations);
}
