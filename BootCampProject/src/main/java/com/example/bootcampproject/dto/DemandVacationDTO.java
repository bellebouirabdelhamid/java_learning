package com.example.bootcampproject.dto;

import com.example.bootcampproject.domain.Employee.Employee;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DemandVacationDTO {
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDateVacation;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date endDateVacation;
    private String employeeReason;
    private String adminStatus;
    private String adminMotif;


}
