package com.example.bootcampproject.dto.Mapper;

import com.example.bootcampproject.domain.Employee.Employee;
import com.example.bootcampproject.dto.EmployeeDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class EmployeeMapper {
    public abstract EmployeeDTO toDto(Employee employee);
//    public abstract List<EmployeeDTO> toDtoList(List<Employee> employees);
}
