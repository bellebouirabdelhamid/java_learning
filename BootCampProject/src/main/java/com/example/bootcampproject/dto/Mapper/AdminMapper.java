package com.example.bootcampproject.dto.Mapper;

import com.example.bootcampproject.domain.Admin.Admin;
import com.example.bootcampproject.dto.AdminDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class AdminMapper {
    public abstract AdminDTO toDto (Admin admin);
    public abstract List<AdminDTO> toDtoList (List<Admin> admins);
}
