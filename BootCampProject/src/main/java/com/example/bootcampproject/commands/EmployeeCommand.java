package com.example.bootcampproject.commands;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeCommand {
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
    private String type;
    private boolean inVacation;
}
