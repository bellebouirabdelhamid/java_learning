package com.example.bootcampproject.commands;


import com.example.bootcampproject.domain.Employee.Employee;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class DemandVacationCommand {
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDateVacation;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date endDateVacation;
    private String employeeReason;
    private String adminStatus;
    private String adminMotif;
    private Employee employee;
}
