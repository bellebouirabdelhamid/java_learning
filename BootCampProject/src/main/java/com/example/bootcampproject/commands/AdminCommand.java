package com.example.bootcampproject.commands;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdminCommand {
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private String password;
}
