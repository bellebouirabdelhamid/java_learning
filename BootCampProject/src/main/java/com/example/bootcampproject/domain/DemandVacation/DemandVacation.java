package com.example.bootcampproject.domain.DemandVacation;

import com.example.bootcampproject.commands.DemandVacationCommand;
import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.Employee.Employee;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="demands")
public class DemandVacation {
    @Id
    private String idDemand;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date startDateVacation;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date endDateVacation;
    private String employeeReason;
    private String adminStatus;
    private String adminMotif;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public static DemandVacation createEmployeeDemand(DemandVacationCommand demandVacationCommand){
        DemandVacation demandVacation = new DemandVacation();
        demandVacation.setIdDemand(UUID.randomUUID().toString());
        demandVacation.setStartDateVacation(demandVacationCommand.getStartDateVacation());
        demandVacation.setEndDateVacation(demandVacationCommand.getEndDateVacation());
        demandVacation.setEmployeeReason(demandVacationCommand.getEmployeeReason());
        demandVacation.setEmployee(demandVacation.getEmployee());
        return demandVacation;
    }

    public void updateEmployeeDemand(DemandVacationCommand demandVacationCommand){
        this.startDateVacation=demandVacationCommand.getStartDateVacation();
        this.endDateVacation=demandVacationCommand.getEndDateVacation();
        this.employeeReason=demandVacationCommand.getEmployeeReason();
        this.adminStatus=demandVacationCommand.getAdminStatus();
        this.adminMotif=demandVacationCommand.getAdminMotif();
    }
}
