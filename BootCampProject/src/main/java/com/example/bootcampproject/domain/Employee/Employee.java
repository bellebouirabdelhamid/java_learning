package com.example.bootcampproject.domain.Employee;

import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.BaseEntity;

import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employees")
public class Employee extends BaseEntity {
    @Column(name = "type_emp")
    private String type;
    @Column(name="in_Vacation")
    private boolean inVacation;
    @Column(name = "vacation_duration_left")
    private float vacationDurationLeft;
    @OneToMany
    private List<DemandVacation> demands;

    public static Employee create(EmployeeCommand employeeCommand){
        Employee employee = new Employee();
        employee.setId(UUID.randomUUID().toString());
        employee.setFirstName(employeeCommand.getFirstName());
        employee.setLastName(employeeCommand.getLastName());
        employee.setEmail(employeeCommand.getEmail());
        employee.setLogin(employeeCommand.getLogin());
        employee.setPassword(employeeCommand.getPassword());
        employee.setType(employeeCommand.getType());
        employee.setInVacation(employeeCommand.isInVacation());
        employee.setVacationDurationLeft(14);
        return employee;
    }

    public void update(final EmployeeCommand employeeCommand){
        this.firstName = employeeCommand.getFirstName();
        this.lastName = employeeCommand.getLastName();
        this.login = employeeCommand.getLogin();
        this.password = employeeCommand.getPassword();
        this.inVacation = employeeCommand.isInVacation();
    }

}


