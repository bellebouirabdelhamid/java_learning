package com.example.bootcampproject.domain.Admin;

import com.example.bootcampproject.commands.AdminCommand;
import com.example.bootcampproject.domain.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="admins")
public class Admin extends BaseEntity {
    public static Admin create(AdminCommand adminCommand){
       Admin admin =new Admin();
       admin.setId(UUID.randomUUID().toString());
       admin.setFirstName(adminCommand.getFirstName());
       admin.setLastName(adminCommand.getLastName());
       admin.setEmail(adminCommand.getEmail());
       admin.setLogin(adminCommand.getLogin());
       admin.setPassword(adminCommand.getPassword());
       return admin;
    }

    public void update(AdminCommand adminCommand){
        this.firstName=adminCommand.getFirstName();
        this.lastName=adminCommand.getLastName();
        this.email=adminCommand.getEmail();
        this.login=adminCommand.getLogin();
        this.password=adminCommand.getPassword();
    }
}
