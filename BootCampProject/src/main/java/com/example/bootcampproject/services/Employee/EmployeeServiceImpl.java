package com.example.bootcampproject.services.Employee;

import com.example.bootcampproject.Repository.EmployeeRepository;
import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.Employee.Employee;
import com.example.bootcampproject.exception.BusinessException;
import com.example.bootcampproject.exception.ExceptionPayloadFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService{

    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getEmployeesList() {
        return employeeRepository.findAll();
    }

    @Override
    public void addEmployee(final EmployeeCommand employeeCommand) {
        employeeRepository.save(Employee.create(employeeCommand));
    }

    @Override
    public Employee getEmployee(String id) {
        return employeeRepository.findById(id).orElseThrow(
                () -> new BusinessException(ExceptionPayloadFactory.EMPLOYEE_NOT_FOUND.get())
        );
    }

    @Override
    public void updateEmployee(final String id,final EmployeeCommand employeeCommand) {
        Employee employee = getEmployee(id);
        employee.update(employeeCommand);
        employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(String id) {
        employeeRepository.deleteById(id);
    }
}
