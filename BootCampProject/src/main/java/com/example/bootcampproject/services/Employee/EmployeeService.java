package com.example.bootcampproject.services.Employee;

import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.Employee.Employee;

import java.util.List;


public interface EmployeeService {
    List<Employee> getEmployeesList();
    void addEmployee (EmployeeCommand employeeCommand);
    Employee getEmployee (String id);
    void updateEmployee(String id, EmployeeCommand employeeCommand);
    void deleteEmployee(String id);
}
