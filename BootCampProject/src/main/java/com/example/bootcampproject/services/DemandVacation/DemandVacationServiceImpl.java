package com.example.bootcampproject.services.DemandVacation;

import com.example.bootcampproject.Repository.DemandVacationRepository;
import com.example.bootcampproject.commands.DemandVacationCommand;
import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import com.example.bootcampproject.domain.Employee.Employee;
import com.example.bootcampproject.dto.Mapper.DemandVacationMapper;
import com.example.bootcampproject.exception.BusinessException;
import com.example.bootcampproject.exception.ExceptionPayloadFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DemandVacationServiceImpl implements DemandVacationService{

    private final DemandVacationRepository demandVacationRepository;

    @Override
    public void addNewDemand(final DemandVacationCommand demandVacationCommand) {
        demandVacationRepository.save(DemandVacation.createEmployeeDemand(demandVacationCommand));
    }

    @Override
    public DemandVacation getDemandVAcation(String id) {
        return demandVacationRepository.findById(id).orElseThrow(
                () -> new BusinessException(ExceptionPayloadFactory.DEMAND_NOT_FOUND.get())
        );
    }

    @Override
    public List<DemandVacation> getListOfDemands() {
        return demandVacationRepository.findAll();
    }

    @Override
    public void updateEmployeeDemand(String id, DemandVacationCommand demandVacationCommand) {
        DemandVacation demandVacation = getDemandVAcation(id);
        demandVacation.updateEmployeeDemand(demandVacationCommand);
        demandVacationRepository.save(demandVacation);
    }

    @Override
    public void deleteDemand(String id) {
        demandVacationRepository.deleteById(id);
    }


}
