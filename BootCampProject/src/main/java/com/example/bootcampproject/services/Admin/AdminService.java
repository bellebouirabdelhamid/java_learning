package com.example.bootcampproject.services.Admin;


import com.example.bootcampproject.commands.AdminCommand;
import com.example.bootcampproject.domain.Admin.Admin;

import java.util.List;

public interface AdminService {
    List<Admin> getListOfAdmins();
    void addAdmin(AdminCommand adminCommand);
    Admin getAdmin(String id);
    void updateAdmin(String id,AdminCommand adminCommand);
    void deleteAdmin(String id);
}
