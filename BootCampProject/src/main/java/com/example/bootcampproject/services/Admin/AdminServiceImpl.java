package com.example.bootcampproject.services.Admin;

import com.example.bootcampproject.Repository.AdminRepository;
import com.example.bootcampproject.commands.AdminCommand;
import com.example.bootcampproject.domain.Admin.Admin;
import com.example.bootcampproject.dto.AdminDTO;
import com.example.bootcampproject.exception.BusinessException;
import com.example.bootcampproject.exception.ExceptionPayloadFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AdminServiceImpl implements AdminService{

    private final AdminRepository adminRepository;

    @Override
    public List<Admin> getListOfAdmins() {
        return adminRepository.findAll();
    }

    @Override
    public void addAdmin(AdminCommand adminCommand) {
        adminRepository.save(Admin.create(adminCommand));
    }

    @Override
    public Admin getAdmin(String id) {
        return adminRepository.findById(id).orElseThrow(
                () -> new BusinessException(ExceptionPayloadFactory.ADMIN_NOT_FOUND.get())
        );

    }

    @Override
    public void updateAdmin(String id, AdminCommand adminCommand) {
        Admin admin= getAdmin(id);
        admin.update(adminCommand);
        adminRepository.save(admin);
    }

    @Override
    public void deleteAdmin(String id) {
        adminRepository.deleteById(id);
    }
}
