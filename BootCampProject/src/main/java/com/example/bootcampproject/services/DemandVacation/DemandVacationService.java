package com.example.bootcampproject.services.DemandVacation;

import com.example.bootcampproject.commands.DemandVacationCommand;
import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import com.example.bootcampproject.domain.Employee.Employee;

import java.util.List;

public interface DemandVacationService {

    void addNewDemand(DemandVacationCommand demandVacationCommand);
    DemandVacation getDemandVAcation(String id);
    List<DemandVacation> getListOfDemands();
    void updateEmployeeDemand(String id,DemandVacationCommand demandVacationCommand);
    void deleteDemand(String id);
}
