package com.example.bootcampproject.Constants;

public class PathVariable {
    public static final String EMPLOYEES = "/employees";
    public static final String ADMINS = "/admins";
    public static final String DEMANDS = "/demands";
}
