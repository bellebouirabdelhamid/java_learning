package com.example.bootcampproject.api;

import com.example.bootcampproject.commands.AdminCommand;
import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.Admin.Admin;
import com.example.bootcampproject.domain.Employee.Employee;
import com.example.bootcampproject.dto.AdminDTO;
import com.example.bootcampproject.dto.Mapper.AdminMapper;
import com.example.bootcampproject.services.Admin.AdminService;
import com.example.bootcampproject.services.Employee.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.bootcampproject.Constants.PathVariable.ADMINS;
import static com.example.bootcampproject.Constants.PathVariable.EMPLOYEES;

@RestController
@RequestMapping(ADMINS)
@RequiredArgsConstructor
public class AdminController {

    private final EmployeeService employeeService;
    private final AdminService adminService;
    private final AdminMapper adminMapper;

    @GetMapping(EMPLOYEES)
    public List<Employee> listOfEmployees()
    {
        return employeeService.getEmployeesList();
    }

    @GetMapping(EMPLOYEES+"/{id}")
    public Employee getEmployee(@PathVariable final String id){
        return employeeService.getEmployee(id);
    }

    @PutMapping(EMPLOYEES+"/{id}")
    public void updateEmployee(@PathVariable final String id,@RequestBody final EmployeeCommand employeeCommand){
        employeeService.updateEmployee(id,employeeCommand);
    }

    @PostMapping(EMPLOYEES)
    public void addEmployee(@RequestBody EmployeeCommand employeeCommand){
        employeeService.addEmployee(employeeCommand);
    }

    @DeleteMapping(EMPLOYEES+"/{id}")
    public void deleteEmployee(@PathVariable String id){
        employeeService.deleteEmployee(id);
    }


    // * *********************************************************************************************
    @GetMapping
    public List<Admin> getAllAdmins(){
     return adminService.getListOfAdmins();
    }

// * using ListDto
//    @GetMapping
//    public List<AdminDTO> getAllAdminsDto(){
//        List<Admin> admins = adminService.getListOfAdmins();
//        return adminMapper.toDtoList(admins);
//    }

    @GetMapping("/{id}")
    public AdminDTO getAdminById(@PathVariable String id){
        return adminMapper.toDto(adminService.getAdmin(id));
    }

    @PostMapping
    public void addAdmin(@RequestBody AdminCommand adminCommand){
        adminService.addAdmin(adminCommand);
    }

    @PutMapping("/{id}")
    public void updateAdmin(@PathVariable String id,@RequestBody AdminCommand adminCommand){
        adminService.updateAdmin(id, adminCommand);
    }

    @DeleteMapping("/{id}")
    public void deleteAdmin(@PathVariable String id){
        adminService.deleteAdmin(id);
    }
}
