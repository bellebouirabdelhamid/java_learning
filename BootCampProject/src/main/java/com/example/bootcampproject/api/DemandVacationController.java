package com.example.bootcampproject.api;

import com.example.bootcampproject.commands.DemandVacationCommand;
import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import com.example.bootcampproject.services.DemandVacation.DemandVacationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.bootcampproject.Constants.PathVariable.DEMANDS;

@RestController
@RequestMapping(DEMANDS)
@RequiredArgsConstructor
public class DemandVacationController {
    private final DemandVacationService demandVacationService;

    @GetMapping
    public List<DemandVacation> getListDemands(){
        return demandVacationService.getListOfDemands();
    }

    @GetMapping("/{id}")
    public DemandVacation getDemand(@PathVariable String id){
        return demandVacationService.getDemandVAcation(id);
    }

    @PostMapping
    public void add(@RequestBody DemandVacationCommand demandVacationCommand){
        demandVacationService.addNewDemand(demandVacationCommand);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable String id,@RequestBody DemandVacationCommand demandVacationCommand)
    {
        demandVacationService.updateEmployeeDemand(id,demandVacationCommand);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id){
        demandVacationService.deleteDemand(id);
    }


}
