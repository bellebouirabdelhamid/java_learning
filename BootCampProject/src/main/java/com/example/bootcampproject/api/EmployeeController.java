package com.example.bootcampproject.api;

import com.example.bootcampproject.commands.EmployeeCommand;
import com.example.bootcampproject.domain.Employee.Employee;
import com.example.bootcampproject.dto.EmployeeDTO;
import com.example.bootcampproject.dto.Mapper.EmployeeMapper;
import com.example.bootcampproject.services.Employee.EmployeeService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static com.example.bootcampproject.Constants.PathVariable.EMPLOYEES;

@RestController
@RequestMapping(EMPLOYEES)
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    private final EmployeeMapper employeeMapper;

//    @GetMapping("/{id}")
//    public Employee getEmployee(@PathVariable String id){
//        return employeeService.getEmployee(id);
//    }

    @GetMapping("/{id}")
    public EmployeeDTO getEmployee(@PathVariable String id){
        return employeeMapper.toDto(employeeService.getEmployee(id));
    }


    @PutMapping("/{id}")
    public void update(@PathVariable String id, @RequestBody EmployeeCommand employeeCommand){
        employeeService.updateEmployee(id, employeeCommand);
    }
}
