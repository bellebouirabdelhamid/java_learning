package com.example.bootcampproject.Repository;

import com.example.bootcampproject.domain.DemandVacation.DemandVacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DemandVacationRepository extends JpaRepository<DemandVacation,String> {
}
