package com.example.bootcampproject.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ExceptionPayloadFactory {

    TECHNICAL_ERROR(0, HttpStatus.INTERNAL_SERVER_ERROR, "technical.error"),
    EMPLOYEE_NOT_FOUND(1, HttpStatus.NOT_FOUND,"employee.not.found"),
    ADMIN_NOT_FOUND(2, HttpStatus.NOT_FOUND,"admin.not.found"),
    DEMAND_NOT_FOUND(3, HttpStatus.NOT_FOUND,"demand.vacation.not.found"),
    MISSING_REQUEST_BODY_ERROR_CODE(4, HttpStatus.BAD_REQUEST, "request.missing.body");

    private final Integer code;
    private final HttpStatus status;
    private final String message;

    public ExceptionPayload get() {
        return new ExceptionPayload(code, status, message);
    }

}

