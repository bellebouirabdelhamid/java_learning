package com.example.springbootstarter.testandlearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestandlearnApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestandlearnApplication.class, args);
	}

}
