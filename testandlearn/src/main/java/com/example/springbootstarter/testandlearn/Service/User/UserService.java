package com.example.springbootstarter.testandlearn.Service.User;

import java.util.List;

import com.example.springbootstarter.testandlearn.commands.UserCommand;
import com.example.springbootstarter.testandlearn.domain.User;

public interface UserService {
    List<User> getAllUsers();

    User getUser(String id);

    void adduser(UserCommand user);

    void updateUser(String id, UserCommand userCommand);

    void deleteuser(String id);
    
}
