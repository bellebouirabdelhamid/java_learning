package com.example.springbootstarter.testandlearn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springbootstarter.testandlearn.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {
    
}
