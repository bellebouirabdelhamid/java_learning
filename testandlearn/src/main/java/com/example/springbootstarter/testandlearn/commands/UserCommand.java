package com.example.springbootstarter.testandlearn.commands;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCommand {
    private String id;
    private String name;
    private String age;
}
