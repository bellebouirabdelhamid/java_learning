package com.example.springbootstarter.testandlearn.Service.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.springbootstarter.testandlearn.commands.UserCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springbootstarter.testandlearn.domain.User;
import com.example.springbootstarter.testandlearn.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        userRepository.findAll()
        .forEach(users::add);
        return users;
    }

    @Override
    public User getUser(String id) {
        return userRepository.findById(id).get();
    }

    @Override
    public void adduser(final UserCommand userCommand) {
        userRepository.save(User.create(userCommand));
    }

    @Override
    public void updateUser(final String id,final UserCommand userCommand) {
        User user = userRepository.findById(id).get();
        user.update(userCommand);
        userRepository.save(user);
    }

    @Override
    public void deleteuser(final String id) {
        User user = userRepository.findById(id).get();
        userRepository.delete(user);
    }
    
}
