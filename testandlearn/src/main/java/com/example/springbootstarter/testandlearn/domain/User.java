package com.example.springbootstarter.testandlearn.domain;

import com.example.springbootstarter.testandlearn.commands.UserCommand;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "usertable", schema = "public")
public class User {
    @Id
    private String id;
    private String name;
    private String age;

    private String chi7aja;

    public static User create(UserCommand userCommand){
        User user = new User();
        user.setId(userCommand.getId());
        user.setName(userCommand.getName());
        user.setAge(userCommand.getAge());
        return user;
    }

    public void update(UserCommand userCommand){
        this.id = userCommand.getId();
        this.name = userCommand.getName();
        this.age = userCommand.getAge();
    }

    
}