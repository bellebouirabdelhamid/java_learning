# Spring Boot learning :

## What is Spring boot ? :

---

- **Spring** : is something must be familiar with.

  - it's the spring framework.
  - It's a a framework which lets you write enterprises job applications.

- **Boot** : is bootstrap.

- **Spring Boot** : is a tool which lets you create spring based application.

## What is Spring ? :

---

- Application framework .
- Programming and configuration model .
- Infrastructure support .

## Maven:

---

- is a build management tool .
- Let's you declare all the dependencies that you in a single file. (pom.xml)

## Controller :

---

- Is basically a java class that has certain annotations marked in it.
  - This annotations lets spring know what is the URL that you are mapping into and what should happen when the request comes to that URL.
